﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    public class UserStatisticModel
    {
        public string Username { get; set; }
        
        public int CarsWins { get; set; }
        public int CarsTotal { get; set; }

        public int AnimalsWins { get; set; }
        public int AnimalsTotal { get; set; }
        
        public int CitiesWins { get; set; }
        public int CitiesTotal { get; set; }
        
        public int MoviesWins { get; set; }
        public int MoviesTotal { get; set; }
        
        public int FoodWins { get; set; }
        public int FoodTotal { get; set; }

        public UserStatisticModel(string username)
        {
            Username = username;
        }
    }
}
