﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    public class GameCategory
    {
        public enum GameCategoryNames
        {
            Cars,
            Animals,
            Movies,
            Cities,
            Food,
        }

        public GameCategoryNames Name { get; set; }
        public string BackgroundPicturePath => Utility.CategoryToBackgroundPicturePath(Name);
        
        public GameCategory(GameCategoryNames name)
        {
            Name = name;
        }
    }

    

}
