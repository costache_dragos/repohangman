﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    public class GameModel
    {
        public GameCategory Category { get; set; }
        public string Word { get; set; }
        public int GuessesLeft { get; set; }
        public int TotalGuesses { get; set; }
        public int NumberOfLettersGuessed { get; set; }
        public uint SecondsLeft { get; set; }
        public ObservableCollection<char> GuessedWordLetters { get; set; }
        public ObservableCollection<char> AvailableLetters { get; set; }

        public int NumberOfLevels { get; set; }
        public int CurrentLevel { get; set; }

        public GameModel(GameCategory category)
        {
            Category = category;
            CurrentLevel = 1;
            NumberOfLevels = Constants.numberOfLevels;
            Word = PickRandomWord();
            BuildGuessedWordLetters();
            GuessesLeft = TotalGuesses = Constants.guessesPerLevel;
            SecondsLeft = Constants.secondsPerLevel;
            NumberOfLettersGuessed = 0;
            AvailableLetters = new ObservableCollection<char>(Constants.alphabet);
        }

        //picks a random word from the words file of the game category
        public string PickRandomWord()
        {
            var wordList = Utility.ReadWordsFromFile(Category.Name);

            if(wordList.Count > 0)
            {
                var random = new Random();
                int randomIndex = random.Next(wordList.Count);
                return wordList[randomIndex].ToUpper();
            }

            return string.Empty;
        }

        //builds a guess word that contains:
        //  '_' at unguessed letters positions
        //  a letter at guessed letters positions
        //  white space at separator positions
        public void BuildGuessedWordLetters()
        {
            GuessedWordLetters = new ObservableCollection<char>();
            foreach (char character in Word)
            {
                if (char.IsLetter(character))
                    GuessedWordLetters.Add('_');
                else if(char.IsSeparator(character))
                    GuessedWordLetters.Add(' ');
            }
        }
    }
}
