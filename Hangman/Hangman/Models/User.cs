﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    public class User
    {
        private string _profilePictureName;

        public string Username { get; set; }
        public string ProfilePictureName
        {
            get { return _profilePictureName; } 
            set { _profilePictureName = value; SaveUserData(); }
        }

        public User(string username, string profilePictureName)
        {
            Username = username;
            ProfilePictureName = profilePictureName;
            SaveUserData();
        }

        public void SaveUserData()
        {
            //create the user directory(nothing happens if it already exists)
            System.IO.Directory.CreateDirectory(Path.Combine(Constants.userDataStoragePath, Username));

            //create a json format using json.net
            string saveContent = JsonConvert.SerializeObject(this);

            //write the save file in the user directory
            System.IO.File.WriteAllText(
                Path.Combine(Constants.userDataStoragePath, Username, Constants.userDataFileName), 
                saveContent);
        }
    }
}
