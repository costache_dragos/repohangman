﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    public class SaveModel
    {
        public GameModel SavedGame { get; set; }
        public string Name { get; set; }
        public string LastWriteTime { get; set; }
    }
}
