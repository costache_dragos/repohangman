﻿using Hangman.Commands;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    public class GameCategorySelectorViewModel : BaseViewModel
    {
        private User _currentUser;
        private GameCategory _selectedCategory;

        public ObservableCollection<GameCategory> GameCategories { get; set; }
        public GameCategory SelectedCategory 
        { 
            get { return _selectedCategory; } 
            set 
            {
                _selectedCategory = value; 
                OnPropertyChanged();
            }
        }

        public User CurrentUser 
        { get { return _currentUser; } set { _currentUser = value; OnPropertyChanged(); } }

        public GameCategorySelectorViewModel()
        {
            GameCategories = new ObservableCollection<GameCategory>();
            
            //loop through all category names and create a game category for each
            foreach (GameCategory.GameCategoryNames gameCategoryName in Enum.GetValues(typeof(GameCategory.GameCategoryNames)))
                GameCategories.Add(new GameCategory(gameCategoryName));
        }
    }
}
