﻿using Hangman.Commands;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    public class MainMenuViewModel : BaseViewModel
    {
        private User _currentUser;
        private string _selectedUserProfilePicturePath;

        public User CurrentUser 
        { 
            get { return _currentUser; } 
            set 
            { 
                _currentUser = value; 
                OnPropertyChanged();

                CurrentUserProfilePicturePath =
                    Utility.ProfilePictureNameToPath(value?.ProfilePictureName);
            } 
        }
        public string CurrentUserProfilePicturePath
        {
            get { return _selectedUserProfilePicturePath; }
            set
            {
                _selectedUserProfilePicturePath = value;
                OnPropertyChanged();

                if (CurrentUser != null)
                    CurrentUser.ProfilePictureName = Path.GetFileName(value);
            }
        }

        public MainMenuViewModel()
        {
        }
    }
}
