﻿using Hangman.Commands;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Hangman.ViewModels
{
    public class GameViewModel : BaseViewModel 
    {
        private GameModel _game;
        private User _currentUser;
        private string _selectedUserProfilePicturePath;
        private bool _isGameOver;
        private DispatcherTimer _timer;
        
        public GameModel Game
        {
            get { return _game; }
            set 
            { 
                _game = value;
                
                InitHealthBar();
                InitTimer();
            }
        }

        public ObservableCollection<char> AvailableLetters => Game.AvailableLetters;
        public ObservableCollection<char> GuessedWordLetters => Game.GuessedWordLetters;
        
        //contains path to empty and full hearts images
        public ObservableCollection<string> HealthBar { get; set; }
        public int GuessesLeft 
        { 
            get { return Game.GuessesLeft; } 
            set { Game.GuessesLeft = value; OnPropertyChanged(); } 
        }
        public uint SecondsLeft
        {
            get { return Game.SecondsLeft; }
            set { Game.SecondsLeft = value; OnPropertyChanged(); }
        }
        public DispatcherTimer Timer { get { return _timer; } set { _timer = value; } }
        public string LevelDisplay 
        {
            get { return $"{Game.CurrentLevel}/{Game.NumberOfLevels}"; }
        }

        //user related
        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                OnPropertyChanged();

                CurrentUserProfilePicturePath =
                    Utility.ProfilePictureNameToPath(value?.ProfilePictureName);
            }
        }
        public string CurrentUserProfilePicturePath
        {
            get { return _selectedUserProfilePicturePath; }
            set
            {
                _selectedUserProfilePicturePath = value;
                OnPropertyChanged();

                if (CurrentUser != null)
                    CurrentUser.ProfilePictureName = Path.GetFileName(value);
            }
        }
        public string GameCategoryPicturePath => Game.Category.BackgroundPicturePath;

        public bool IsGameOver 
        { 
            get { return _isGameOver; } 
            set { _isGameOver = value; OnPropertyChanged(); } 
        }

        public ICommand LetterSelectedCommand { get; }
        public GameViewModel()
        {
            LetterSelectedCommand = new LetterSelectedCommand(this);
        }

        //initializes the healthbar heart paths depending of total guesses and guesses left
        private void InitHealthBar()
        {
            HealthBar = new ObservableCollection<string>(
                    Enumerable.Repeat(Utility.HealthBarImagePath(false), Game.GuessesLeft));
            
            for (int i = 0; i < Game.TotalGuesses - Game.GuessesLeft; i++)
            {
                HealthBar.Add(Utility.HealthBarImagePath(true));
            }

            OnPropertyChanged(nameof(HealthBar));
        }

        #region Timer
        //initializes the timer
        private void InitTimer()
        {
            Timer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(1)
            };
            Timer.Tick += Timer_Tick;
            Timer.Start();
        }

        //executes at every tick of the timer (every second for our purpose)
        private void Timer_Tick(object sender, EventArgs e)
        {
            SecondsLeft--;
            if (SecondsLeft == 0)
            {
                MessageBox.Show($"You ran out of time! The word is {Game.Word}",
                            "Defeat..", MessageBoxButton.OK, MessageBoxImage.Information);
                IsGameOver = true;
                Timer.Stop();
            }
        }
        #endregion

        //picks a new word and does the setup for a new level of the game
        public void NextLevel()
        {
            Game.CurrentLevel++;
            OnPropertyChanged(nameof(LevelDisplay));
            
            //we make sure that the same word is not picked twice in a row
            string newWord;
            do
            {
                newWord = Game.PickRandomWord();
            } while (newWord == Game.Word);
            Game.Word = newWord;
            
            Game.NumberOfLettersGuessed = 0;
            GuessesLeft = Constants.guessesPerLevel;
            SecondsLeft = Constants.secondsPerLevel;
            Game.BuildGuessedWordLetters();
            Game.AvailableLetters = new ObservableCollection<char>(Constants.alphabet);

            //notify the UI to update the available letters and guessed word letters
            OnPropertyChanged(nameof(AvailableLetters));
            OnPropertyChanged(nameof(GuessedWordLetters));

            InitHealthBar();
            InitTimer();
        }
    }
}
