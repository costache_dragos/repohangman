﻿using Hangman.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    public class NewUserWindowViewModel : BaseViewModel, Utility.ICloseWindows
    {
        private string _inputUsername;
        public string InputUsername 
        { 
            get { return _inputUsername; } 
            set { _inputUsername = value; OnPropertyChanged(); } 
        }
        public ProfileSelectorViewModel ProfileSelectorViewModel { get; set; }

        public ICommand AddUserCommand { get; }
        public Action Close { get; set; }

        public NewUserWindowViewModel()
        {
            InputUsername = string.Empty;
            AddUserCommand = new AddUserCommand(this);
        }

        public void CloseWindow()
        {
            //if the close action has subscribers, it calls them, closing the 
            //windows that use this viewModel (see NewUserView code-behind)
            Close?.Invoke();
        }
    }
}
