﻿using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.ViewModels
{
    public class StatisticsViewModel : BaseViewModel
    {
        public List<UserStatisticModel> Stats { get; set; }
        public User CurrentUser { get; set; }
        
        public StatisticsViewModel()
        {
            Stats = Utility.LoadStatistics();
        }
    }
}
