﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Hangman.Models;
using System.IO;

namespace Hangman.ViewModels
{
    public class ProfileSelectorViewModel : BaseViewModel
    {
        private ObservableCollection<User> _users;
        private readonly List<string> _profilePictures;
        private User _selectedUser;
        private string _selectedUserProfilePicturePath;

        public ObservableCollection<User> Users 
        { 
            get { return _users; } 
            set { _users = value; }
        }
        public User SelectedUser 
        { 
            get { return _selectedUser; } 
            set 
            { 
                _selectedUser = value; 
                OnPropertyChanged();
                SelectedUserProfilePicturePath = 
                    Utility.ProfilePictureNameToPath(value?.ProfilePictureName);
            }
        }
        public List<string> ProfilePictures { get { return _profilePictures; } }
        public string SelectedUserProfilePicturePath
        {
            get { return _selectedUserProfilePicturePath; }
            set 
            { 
                _selectedUserProfilePicturePath = value; 
                OnPropertyChanged(); 

                if(SelectedUser != null)
                    SelectedUser.ProfilePictureName = Path.GetFileName(value);
            }
        }

        public ICommand NextPictureCommand { get; }
        public ICommand PrevPictureCommand { get; }
        public ICommand NewUserCommand { get; }
        public ICommand DeleteUserCommand { get; }

        public ProfileSelectorViewModel()
        {
            _profilePictures = Utility.LoadProfilePicturesPaths();
            NextPictureCommand = new Commands.PrevOrNextPictureCommand(this);
            PrevPictureCommand = new Commands.PrevOrNextPictureCommand(this);
            NewUserCommand = new Commands.OpenNewUserWindowCommand(this);
            DeleteUserCommand = new Commands.DeleteUserCommand(this);

            Users = Utility.LoadUsersData();
            SelectedUserProfilePicturePath = Utility.ProfilePictureNameToPath(Constants.defaultProfilePictureName);
        }

        public void AddUser(string username)
        {
            User newUser = new User(username, Constants.defaultProfilePictureName);
            Users.Add(newUser);
            SelectedUser = newUser;
        }
    }
}
