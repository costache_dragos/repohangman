﻿using Hangman.Commands;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    public class NewSaveWindowViewModel : BaseViewModel, Utility.ICloseWindows
    {
        private string _inputSaveName;
        public string InputSaveName 
        { 
            get { return _inputSaveName; } 
            set { _inputSaveName = value; OnPropertyChanged(); } 
        }
        public SaveGameViewModel SaveGameViewModel { get; set; }

        public Action Close { get; set; }
        
        public ICommand CreateNewSaveCommand { get; }
        public NewSaveWindowViewModel()
        {
            CreateNewSaveCommand = new CreateNewSaveCommand(this);
        }
        
        public void CloseWindow()
        {
            //if the close action has subscribers, it calls them, closing the 
            //windows that use this viewModel (see NewUserView code-behind)
            Close?.Invoke();
        }
    }
}
