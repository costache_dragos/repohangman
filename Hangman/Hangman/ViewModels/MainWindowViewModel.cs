﻿using Hangman.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    public class MainWindowViewModel : BaseViewModel, Utility.ICloseWindows
    {
        private BaseViewModel _currentViewModel;
        public BaseViewModel CurrentViewModel
        {
            get { return _currentViewModel; }
            set 
            { 
                _currentViewModel = value; 
                OnPropertyChanged(); 
                //any time we change the viewModel we need a clean navigation infrastructure
                InitNavigationCommands(); 
            }
        }

        private int _windowWidth;
        private int _windowHeight;
        public int WindowWidth
        {
            get { return _windowWidth; }
            set { _windowWidth = value; OnPropertyChanged(); }
        }
        public int WindowHeight
        {
            get { return _windowHeight; }
            set { _windowHeight = value; OnPropertyChanged(); }
        }

        //all commands that change the view need to be in the mainWindowViewModel in order
        //to have access to the 'currentViewModel' property

        //profile selector
        public ICommand PlayCommand { get; private set; }
        //main menu
        public ICommand NewGameCommand { get; private set; }
        public ICommand MainMenuLoadGameCommand { get; private set; }
        public ICommand NavToStatisticsCommand { get; private set; }
        public ICommand LogOutCommand { get; private set; }
        public ICommand CloseWindowCommand { get; private set; }
        //category selector
        public ICommand PickCategoryCommand { get; private set; }
        //load menu
        public ICommand LoadGameCommand { get; private set; }
        //game
        public ICommand MainMenuCommand { get; private set; }
        public ICommand NavToSavesCommand { get; private set; }
        //save menu
        public ICommand NavBackToGameFromSave { get; private set; }
        public Action Close { get; set; }

        public MainWindowViewModel()
        {
            WindowWidth = Constants.DefaultWindowSize.windowWidth;
            WindowHeight = Constants.DefaultWindowSize.windowHeight;

            InitNavigationCommands();

            CurrentViewModel = new ProfileSelectorViewModel();
        }

        public void CloseWindow()
        {
            //if the close action has subscribers, it calls them, closing the 
            //windows that use this viewModel (see MainWindow code-behind)
            Close?.Invoke();
        }

        private void InitNavigationCommands()
        {
            PlayCommand = new PlayCommand(this);
            NewGameCommand = new NewGameCommand(this);
            MainMenuLoadGameCommand = new MainMenuLoadGameCommand(this);
            LoadGameCommand = new LoadGameCommand(this);
            PickCategoryCommand = new PickCategoryCommand(this);
            NavToStatisticsCommand = new NavToStatisticsCommand(this);
            LogOutCommand = new LogOutCommand(this);
            CloseWindowCommand = new CloseWindowCommand(this);
            MainMenuCommand = new MainMenuCommand(this);
            NavToSavesCommand = new NavToSavesCommand(this);
            NavBackToGameFromSave = new NavBackToGameFromSave(this);
        }
    }
}
