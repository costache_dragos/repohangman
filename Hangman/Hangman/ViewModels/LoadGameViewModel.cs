﻿using Hangman.Commands;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    public class LoadGameViewModel : BaseViewModel
    {
        private User _user;
        private SaveModel _selectedSave;
        public User CurrentUser 
        { 
            get { return _user; }
            set { _user = value; SavedGames = Utility.ReadSaveFiles(value.Username); }
        }
        public ObservableCollection<SaveModel> SavedGames { get; private set; }
        public SaveModel SelectedSave 
        {
            get { return _selectedSave; }
            set { _selectedSave = value; OnPropertyChanged(); }
        }

        public ICommand DeleteSaveCommand { get; }
        public LoadGameViewModel()
        {
            DeleteSaveCommand = new DeleteSaveCommand(this);
        }
    }
}
