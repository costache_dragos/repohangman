﻿using Hangman.Commands;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hangman.ViewModels
{
    public class SaveGameViewModel : BaseViewModel
    {
        private User _user;
        private SaveModel _selectedSave;
        public User CurrentUser
        {
            get { return _user; }
            set { _user = value; SavedGames = Utility.ReadSaveFiles(value.Username); }
        }
        public GameModel GameToBeSaved { get; set; }
        public ObservableCollection<SaveModel> SavedGames { get; set; }
        public SaveModel SelectedSave 
        {
            get { return _selectedSave; }
            set { _selectedSave = value; OnPropertyChanged(); }
        }

        public ICommand OverwriteGameSaveCommand { get; }
        public ICommand NewSaveCommand { get; }
        public ICommand DeleteSaveCommand { get; }
        
        public SaveGameViewModel()
        {
            OverwriteGameSaveCommand = new OverwriteGameSaveCommand(this);
            NewSaveCommand = new OpenNewSaveWindowCommand(this);
            DeleteSaveCommand = new DeleteSaveCommand(this);
        }
    }
}
