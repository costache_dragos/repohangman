﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    public static class Constants
    {
        #region Paths
        public const string profilePicturesStoragePath = @"Res\Profile_pictures";
        public const string userDataStoragePath = @"Res\Users\";
        public const string gameSaveDirName = "Saves";
        public const string categoryDirPath = @"Res\Game_categories";
        public const string userDataFileName = "user-data.json";
        public const string defaultProfilePictureName = "default-profile-picture.jpg";
        public const string statisticsFileName = "statistics.json";
        public const string fullHeartPicturePath = @"Res\Others\FullHeart.jpg";
        public const string emptyHeartPicturePath = @"Res\Others\EmptyHeart.jpg";
        #endregion

        #region Window Properties
        public struct DefaultWindowSize
        {
            public const int windowWidth = 650;
            public const int windowHeight = 450;
        }
        public struct GameWindowSize
        {
            public const int windowWidth = 1400;
            public const int windowHeight = 600;
        }
        #endregion

        #region Const Strings
        public const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        #endregion

        #region Game Config
        public const int guessesPerLevel = 6;
        public const int numberOfLevels = 5;
        public const uint secondsPerLevel = 60;
        #endregion
    }
}
