﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for NewUserView.xaml
    /// </summary>
    public partial class NewUserWindowView : Window
    {
        public NewUserWindowView(ProfileSelectorViewModel parentViewModel)
        {
            InitializeComponent();
            //in order to update the user list live, we need the profile selector view model
            ((NewUserWindowViewModel)DataContext).ProfileSelectorViewModel = parentViewModel;

            Loaded += NewUserWindow_Loaded;
        }

        //if the view model implements the ICloseWindows interface
        //then we subscribe a lambda function that closes
        //this window to the "Close" action from the viewModel
        private void NewUserWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if(DataContext is Utility.ICloseWindows viewModel)
                viewModel.Close += () => { this.Close(); }; 
        }
    }
}
