﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for NewSaveWindowView.xaml
    /// </summary>
    public partial class NewSaveWindowView : Window
    {
        public NewSaveWindowView(SaveGameViewModel parentViewModel)
        {
            InitializeComponent();

            //in order to update the user list live, we need the profile selector view model
            ((NewSaveWindowViewModel)DataContext).SaveGameViewModel = parentViewModel;

            Loaded += NewSaveWindow_Loaded;
        }

        private void NewSaveWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext is Utility.ICloseWindows viewModel)
                viewModel.Close += () => { this.Close(); };
        }
    }
}
