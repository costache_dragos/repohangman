﻿using Hangman.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hangman
{
    public static class Utility
    {
        #region IO
        //loads profile picture paths
        public static List<string> LoadProfilePicturesPaths()
        {
            var pictures = new List<string>();

            foreach (var picture in Directory.GetFiles(Constants.profilePicturesStoragePath))
                pictures.Add(Path.GetFileName(picture));
            
            return pictures;
        }

        //load users
        public static ObservableCollection<User>LoadUsersData()
        {
            var users = new ObservableCollection<User>();
            foreach (var userDirectory in Directory.GetDirectories(Constants.userDataStoragePath))
            {
                string userFilePath = Path.Combine(userDirectory, Constants.userDataFileName);
                if (System.IO.File.Exists(userFilePath))
                {
                    string content = System.IO.File.ReadAllText(userFilePath);
                    users.Add(JsonConvert.DeserializeObject<User>(content));
                }
            }
            return users;
        }

        //reads the statistics file
        public static List<UserStatisticModel> LoadStatistics()
        {
            string statisticsFilePath = Path.Combine(Constants.userDataStoragePath, Constants.statisticsFileName);
            if (System.IO.File.Exists(statisticsFilePath))
            {
                string content = System.IO.File.ReadAllText(statisticsFilePath);
                return JsonConvert.DeserializeObject<List<UserStatisticModel>>(content);
            }
            return new List<UserStatisticModel>();
        }
        //writes the statistics file
        public static void WriteStatistics(List<UserStatisticModel> statistics)
        {
            string statisticsFilePath = Path.Combine(Constants.userDataStoragePath, Constants.statisticsFileName);
            string content = JsonConvert.SerializeObject(statistics);
            System.IO.File.WriteAllText(statisticsFilePath, content);
        }

        //reads all save files of a given user
        public static ObservableCollection<SaveModel>ReadSaveFiles(string username)
        {
            var saves = new ObservableCollection<SaveModel>();

            try
            {
                string saveDirPath = UsernameToUserSaveGamesDirPath(username);
                foreach (var saveFile in Directory.GetFiles(saveDirPath))
                {
                    string content = System.IO.File.ReadAllText(saveFile);
                    GameModel savedGame = JsonConvert.DeserializeObject<GameModel>(content);
                    if (savedGame != null)//checks the integrity of the save file
                    {
                        SaveModel currentSave = new SaveModel()
                        {
                            Name = Path.GetFileNameWithoutExtension(saveFile),
                            SavedGame = savedGame,
                            LastWriteTime = (new FileInfo(saveFile)).LastWriteTime.ToString()
                        };
                        saves.Add(currentSave);
                    }
                }
            }
            catch (Exception)
            {}
            
            return saves;
        }

        //is given a game category and returns the words from the category words file
        public static List<string> ReadWordsFromFile(GameCategory.GameCategoryNames categoryName)
        {
            string wordsFilePath = Path.Combine(Directory.GetCurrentDirectory(),
                Constants.categoryDirPath, categoryName.ToString(),
                $"{categoryName}.txt");
            try
            {
                return System.IO.File.ReadAllLines(wordsFilePath).ToList();
            }
            catch (Exception)
            {
                return new List<string>();
            }
        }
        #endregion

        #region Path Manipulation
        //gets a picture name and appends the full path to it
        public static string ProfilePictureNameToPath(string pictureName)
        {
            try
            {
                return Path.Combine(Directory.GetCurrentDirectory(),
                    Constants.profilePicturesStoragePath,
                    pictureName);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        //gets the path of a certain user directory when given the user name
        public static string UsernameToUserDirPath(string username)
        {
            try
            {
                return Path.Combine(Directory.GetCurrentDirectory(),
                    Constants.userDataStoragePath,
                    username);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        //gets the path of a category background picture when given the category
        public static string CategoryToBackgroundPicturePath(GameCategory.GameCategoryNames categoryName)
        {
            try
            {
                return Path.Combine(Directory.GetCurrentDirectory(),
                    Constants.categoryDirPath,
                    categoryName.ToString(), $"{categoryName}Background.jpg");
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        //gets the relative path of healthbar images
        public static string HealthBarImagePath(bool isEmptyHeart)
        {
            try
            {
                if(isEmptyHeart)
                    return Path.Combine(Directory.GetCurrentDirectory(),
                        Constants.emptyHeartPicturePath);
                else
                    return Path.Combine(Directory.GetCurrentDirectory(),
                        Constants.fullHeartPicturePath);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        //given a username, the function builds the path to the user save games directory
        public static string UsernameToUserSaveGamesDirPath(string username)
        {
            try
            {
                return Path.Combine(Directory.GetCurrentDirectory(),
                    Constants.userDataStoragePath,
                    username,
                    Constants.gameSaveDirName);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region String Manipulation
        //checks if a given username is valid
        public static bool CheckIfUsernameIsValid(string username)
        {
            return Regex.IsMatch(username, "^[a-zA-Z0-9_-]+$");
        }

        //check if save file name is valid
        public static bool CheckIfSaveFileNameIsValid(string saveFileName)
        {
            return Regex.IsMatch(saveFileName, "^[- _]?[a-zA-Z0-9][- a-zA-Z0-9_]*$");
        }

        //finds all indexes of a given character in a string
        public static List<int> FindAllIndexesOfChar(string str, char c)
        {
            var indexes = new List<int>();
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == c)
                    indexes.Add(i);
            }
            return indexes;
        }
        #endregion

        #region Others
        //interface that is used to close windows from view model
        public interface ICloseWindows
        {
            Action Close { get; set; }
            void CloseWindow();
        }

        //adds a game played to the statistics of a player, it also adds a win if it is the case
        public static void UpdateStatistics(string username, GameCategory gameCategory, bool addWin)
        {
            List<UserStatisticModel> stats = LoadStatistics();
            foreach (UserStatisticModel stat in stats)
            {
                if (stat.Username == username)
                {
                    switch(gameCategory.Name)
                    {
                        case GameCategory.GameCategoryNames.Animals:
                            stat.AnimalsTotal++;
                            if (addWin)
                                stat.AnimalsWins++;
                            break;
                        case GameCategory.GameCategoryNames.Cities:
                            stat.CitiesTotal++;
                            if (addWin)
                                stat.CitiesWins++;
                            break;
                        case GameCategory.GameCategoryNames.Movies:
                            stat.MoviesTotal++;
                            if (addWin)
                                stat.MoviesWins++;
                            break;
                        case GameCategory.GameCategoryNames.Cars:
                            stat.CarsTotal++;
                            if (addWin)
                                stat.CarsWins++;
                            break;
                        case GameCategory.GameCategoryNames.Food:
                            stat.FoodTotal++;
                            if (addWin)
                                stat.FoodWins++;
                            break;
                    }
                    break;
                }
            }
            WriteStatistics(stats);
        }
        #endregion
    }
}
