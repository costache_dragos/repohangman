﻿using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hangman.Commands
{
    public class DeleteSaveCommand : CommandBase 
    {
        //accepts LoadViewModel or SaveViewModel
        private readonly BaseViewModel _viewModel; 
        public DeleteSaveCommand(BaseViewModel viewModel)
        {
            _viewModel = viewModel;
            _viewModel.PropertyChanged += OnViewModelPropertyChanged;
        }

        public override void Execute(object parameter)
        {
            if (_viewModel is LoadGameViewModel loadGameViewModel)
            {
                string filePath = 
                    Path.Combine(Utility.UsernameToUserSaveGamesDirPath(loadGameViewModel.CurrentUser.Username),
                    $"{loadGameViewModel.SelectedSave.Name}.save");

                try
                {
                    File.Delete(filePath);
                    loadGameViewModel.SavedGames.Remove(loadGameViewModel.SelectedSave);
                    loadGameViewModel.SelectedSave = null;
                }
                catch (Exception)
                {
                    MessageBox.Show("Could not delete save");
                }
            }
            else if (_viewModel is SaveGameViewModel saveGameViewModel)
            {
                string filePath =
                    Path.Combine(Utility.UsernameToUserSaveGamesDirPath(saveGameViewModel.CurrentUser.Username),
                    $"{saveGameViewModel.SelectedSave.Name}.save");

                try
                {
                    File.Delete(filePath);
                    saveGameViewModel.SavedGames.Remove(saveGameViewModel.SelectedSave);
                    saveGameViewModel.SelectedSave = saveGameViewModel.SavedGames.FirstOrDefault();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        public override bool CanExecute(object parameter)
        {
            if(_viewModel is LoadGameViewModel loadGameViewModel)
                return loadGameViewModel.SelectedSave != null && base.CanExecute(parameter);
            else if (_viewModel is SaveGameViewModel saveGameViewModel)
                return saveGameViewModel.SelectedSave != null && base.CanExecute(parameter);

            return false;
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LoadGameViewModel.SelectedSave) 
                || e.PropertyName == nameof(SaveGameViewModel.SelectedSave))
                OnCanExecuteChanged();
        }
    }
}
