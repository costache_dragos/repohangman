﻿using Hangman.Models;
using Hangman.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class OverwriteGameSaveCommand : CommandBase
    {
        private readonly SaveGameViewModel _saveGameViewModel;
        public OverwriteGameSaveCommand(SaveGameViewModel saveGameViewModel)
        {
            _saveGameViewModel = saveGameViewModel;
            _saveGameViewModel.PropertyChanged += OnViewModelPropertyChanged;
        }

        public override void Execute(object parameter)
        {
            string saveDirPath = Utility.UsernameToUserSaveGamesDirPath(_saveGameViewModel.CurrentUser.Username);
            if (string.IsNullOrEmpty(saveDirPath))
                return;
            
            //create the user directory(nothing happens if it already exists)
            System.IO.Directory.CreateDirectory(saveDirPath);

            //create a json format using json.net
            string saveContent = JsonConvert.SerializeObject(_saveGameViewModel.GameToBeSaved);


            //write the save file in the user directory
            string saveFilePath = Path.Combine(saveDirPath, $"{_saveGameViewModel.SelectedSave.Name}.save");
            System.IO.File.WriteAllText(saveFilePath, saveContent);

            //update the save list
            SaveModel newSave = new SaveModel()
            {
                Name = _saveGameViewModel.SelectedSave.Name,
                SavedGame = _saveGameViewModel.GameToBeSaved,
                LastWriteTime = (new FileInfo(saveFilePath)).LastWriteTime.ToString()
            };

            int selectedIndex = _saveGameViewModel.SavedGames.IndexOf(_saveGameViewModel.SelectedSave);
            _saveGameViewModel.SavedGames.RemoveAt(selectedIndex);
            _saveGameViewModel.SelectedSave = newSave;
            _saveGameViewModel.SavedGames.Insert(selectedIndex, newSave);
        }

        public override bool CanExecute(object parameter)
        {
            return _saveGameViewModel.SelectedSave != null && base.CanExecute(parameter);
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SaveGameViewModel.SelectedSave))
                OnCanExecuteChanged();
        }
    }
}
