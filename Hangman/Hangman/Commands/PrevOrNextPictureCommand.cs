﻿using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class PrevOrNextPictureCommand : CommandBase
    {
        private readonly ProfileSelectorViewModel _viewModel;
        public PrevOrNextPictureCommand(ProfileSelectorViewModel profileSelectorViewModel)
        {
            _viewModel = profileSelectorViewModel;
            _viewModel.PropertyChanged += OnViewModelPropertyChanged;
        }

        //the paramater is going to be a string that takes the value "-1" if we want to go to
        //the previous picture or "1" if we want to go to the next picture
        public override void Execute(object parameter)
        {
            int prevOrNextFlag = int.Parse(parameter.ToString());

            if (_viewModel.SelectedUser == null)
                return;

            //getting the index of the current picture in the list of pictures
            string selectedProfilePictureName = _viewModel.SelectedUser.ProfilePictureName;
            int selectedProfilePictureIndex = _viewModel.ProfilePictures.IndexOf(selectedProfilePictureName);

            if (selectedProfilePictureIndex == -1)
                return;

            selectedProfilePictureIndex += prevOrNextFlag;
            if (selectedProfilePictureIndex >= _viewModel.ProfilePictures.Count)
                selectedProfilePictureIndex = 0;
            else if (selectedProfilePictureIndex < 0)
                selectedProfilePictureIndex = _viewModel.ProfilePictures.Count - 1;

            _viewModel.SelectedUserProfilePicturePath = 
                Utility.ProfilePictureNameToPath(_viewModel.ProfilePictures[selectedProfilePictureIndex]);
        }

        public override bool CanExecute(object parameter)
        {
            return _viewModel.SelectedUser != null && base.CanExecute(parameter);
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ProfileSelectorViewModel.SelectedUser))
                OnCanExecuteChanged();
        }
    }
}
