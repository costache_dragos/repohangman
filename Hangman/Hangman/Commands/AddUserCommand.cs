﻿using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hangman.Commands
{
    public class AddUserCommand : CommandBase
    {
        private readonly NewUserWindowViewModel _newUserViewModel;

        public AddUserCommand(NewUserWindowViewModel newUserViewModel)
        {
            _newUserViewModel = newUserViewModel;
            _newUserViewModel.PropertyChanged += OnViewModelPropertyChanged;
        }

        public override void Execute(object parameter)
        {
            if (!Utility.CheckIfUsernameIsValid(_newUserViewModel.InputUsername))
            {
                MessageBox.Show("Username can only contain letters, digits and underscore!",
                    "Invalid Username", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //check if a user with the same username already exists
            foreach (User user in _newUserViewModel.ProfileSelectorViewModel.Users)
                if (string.Compare(user.Username, _newUserViewModel.InputUsername, true) == 0)
                {
                    MessageBox.Show("A user with the same username already exists!",
                        "Invalid Username", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            
            //create a user using method implemented in the profile selector view model
            _newUserViewModel.ProfileSelectorViewModel.AddUser(_newUserViewModel.InputUsername);

            //get the statistics list and add a new entry for the new user
            List<UserStatisticModel> stats = Utility.LoadStatistics();
            stats.Add(new UserStatisticModel(_newUserViewModel.InputUsername)
            {
                CarsWins = 0, CarsTotal = 0,
                AnimalsWins = 0, AnimalsTotal = 0,
                CitiesWins = 0, CitiesTotal = 0,
                FoodWins = 0, FoodTotal = 0,
                MoviesWins = 0, MoviesTotal = 0,
            });
            Utility.WriteStatistics(stats);

            //after we add the user, we close the window
            _newUserViewModel.CloseWindow();
        }

        public override bool CanExecute(object parameter)
        {
            //if the user hasn't written any username, the command can't execute
            return !string.IsNullOrEmpty(_newUserViewModel.InputUsername) && base.CanExecute(parameter);
        }

        //method is subscribed to the property changed event in the 
        //parent viewModel, it is used to identify when the username that the
        //user inputs has changed and if it has, verify if the command can execute
        //after the username change
        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(NewUserWindowViewModel.InputUsername))
                OnCanExecuteChanged();
        }
    }
}
