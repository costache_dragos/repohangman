﻿using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hangman.Commands
{
    public class DeleteUserCommand : CommandBase
    {
        private readonly ProfileSelectorViewModel _viewModel;
        public DeleteUserCommand(ProfileSelectorViewModel profileSelectorViewModel)
        {
            _viewModel = profileSelectorViewModel;
            _viewModel.PropertyChanged += OnViewModelPropertyChanged;
        }

        public override void Execute(object parameter)
        {
            if(_viewModel.SelectedUser == null)
                return;

            try
            {
                //get the statistics file and remove the user that we are deleting
                List<UserStatisticModel> stats = Utility.LoadStatistics();
                stats.RemoveAll(x => x.Username == _viewModel.SelectedUser.Username);
                Utility.WriteStatistics(stats);

                //delete the user directory and remove the user from the display list
                Directory.Delete(Utility.UsernameToUserDirPath(_viewModel.SelectedUser.Username), true);
                _viewModel.Users.Remove(_viewModel.SelectedUser);
                _viewModel.SelectedUser = _viewModel.Users.FirstOrDefault();
            }
            catch (Exception) 
            {
                MessageBox.Show("Could not delete user");
            }
        }

        public override bool CanExecute(object parameter)
        {
            return _viewModel.SelectedUser != null && base.CanExecute(parameter);
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(ProfileSelectorViewModel.SelectedUser))
                OnCanExecuteChanged();
        }
    }
}
