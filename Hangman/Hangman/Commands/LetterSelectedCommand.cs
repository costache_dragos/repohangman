﻿using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hangman.Commands
{
    public class LetterSelectedCommand : CommandBase
    {
        private readonly GameViewModel _gameViewModel;
        public LetterSelectedCommand(GameViewModel gameViewModel)
        {
            _gameViewModel = gameViewModel;
        }

        //the given parameter is going to be the letter that was selected
        //so we remove it from the available letters 
        //and check if the letter is in the word
        //and update the guessed word letters if it is
        public override void Execute(object parameter)
        {
            if (parameter is char letter)
            {
                //remove the selected letter from the available letters collection
                _gameViewModel.AvailableLetters.Remove(letter);

                //find all (if any) occurances of the letter are in the word
                List<int> letterOccurencesIndexes = 
                    Utility.FindAllIndexesOfChar(_gameViewModel.Game.Word, letter);

                if (letterOccurencesIndexes.Count == 0)
                {
                    //decrease the number of lives
                    _gameViewModel.GuessesLeft--;
                    //update the visual health bar to change the last full heart to an empty heart
                    _gameViewModel.HealthBar[_gameViewModel.GuessesLeft] = Utility.HealthBarImagePath(true);
                    
                    if (_gameViewModel.GuessesLeft == 0)
                    {
                        _gameViewModel.Timer.Stop();
                        MessageBox.Show($"You lost! The word is {_gameViewModel.Game.Word}", 
                            "Defeat..", MessageBoxButton.OK, MessageBoxImage.Information);
                        
                        //update the game statistic
                        Utility.UpdateStatistics(_gameViewModel.CurrentUser.Username, _gameViewModel.Game.Category, false);
                        _gameViewModel.IsGameOver = true;
                    }
                    return;
                }

                //update the displayed word with the guessed letters
                foreach (int index in letterOccurencesIndexes)
                {
                    _gameViewModel.GuessedWordLetters[index] = letter;
                }

                //we update the number of guessed letters and if it equals the word length
                //then the game is won
                _gameViewModel.Game.NumberOfLettersGuessed += letterOccurencesIndexes.Count;

                if (_gameViewModel.Game.NumberOfLettersGuessed == _gameViewModel.Game.Word.Count(char.IsLetter))
                {
                    _gameViewModel.Timer.Stop();
                    if (_gameViewModel.Game.CurrentLevel == _gameViewModel.Game.NumberOfLevels)
                    {
                        MessageBox.Show("You won!", "Victory!", MessageBoxButton.OK, MessageBoxImage.Information);
                        
                        //add the win to the player's statistics
                        Utility.UpdateStatistics(_gameViewModel.CurrentUser.Username, _gameViewModel.Game.Category, true);
                        _gameViewModel.IsGameOver = true;
                    }
                    else
                        _gameViewModel.NextLevel();
                }
            }
        }
    }
}
