﻿using Hangman.ViewModels;
using Hangman.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class OpenNewSaveWindowCommand : CommandBase
    {
        private readonly SaveGameViewModel _saveGameViewModel;
        public OpenNewSaveWindowCommand(SaveGameViewModel saveGameViewModel)
        {
            _saveGameViewModel = saveGameViewModel;
        }

        public override void Execute(object parameter)
        {
            NewSaveWindowView newSaveWindowView = new NewSaveWindowView(_saveGameViewModel);
            newSaveWindowView.ShowDialog();
        }
    }
}
