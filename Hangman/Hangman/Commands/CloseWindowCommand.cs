﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Hangman.Utility;

namespace Hangman.Commands
{
    public class CloseWindowCommand : CommandBase
    {
        private ICloseWindows _closeWindowsViewModel;
        public CloseWindowCommand(ICloseWindows closeWindowsViewModel)
        {
            _closeWindowsViewModel = closeWindowsViewModel;
        }

        public override void Execute(object parameter)
        {
            _closeWindowsViewModel?.CloseWindow();
        }
    }
}
