﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    //the command will take us to the main menu from other views
    //it will recieve a view model that contains an user as a parameter
    //in order to pass the current user to the main menu viewModel
    
    //it is able to be subscribe to the property changed event
    //of a game view model (this will allow the game view model to 
    //trigger this command when the game is over without needing user input)
    public class MainMenuCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        private bool _isAssignedToGame;
        public MainMenuCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
            _isAssignedToGame = false;
        }

        public override void Execute(object parameter)
        {
            if (parameter is GameViewModel gameViewModel)
            {
                gameViewModel.Timer.Stop();
                _mainWindowViewModel.CurrentViewModel = new MainMenuViewModel()
                {
                    CurrentUser = gameViewModel.CurrentUser
                };
            }
            else if(parameter is LoadGameViewModel loadGameViewModel)
            {
                _mainWindowViewModel.CurrentViewModel = new MainMenuViewModel()
                {
                    CurrentUser = loadGameViewModel.CurrentUser
                };
            }
            else if(parameter is GameCategorySelectorViewModel gameCategorySelectorViewModel)
            {
                _mainWindowViewModel.CurrentViewModel = new MainMenuViewModel()
                {
                    CurrentUser = gameCategorySelectorViewModel.CurrentUser
                };
            }
            else if (parameter is StatisticsViewModel statisticsViewModel)
            {
                _mainWindowViewModel.CurrentViewModel = new MainMenuViewModel()
                {
                    CurrentUser = statisticsViewModel.CurrentUser
                };
            }
            _mainWindowViewModel.WindowWidth = Constants.DefaultWindowSize.windowWidth;
            _mainWindowViewModel.WindowHeight = Constants.DefaultWindowSize.windowHeight;
        }

        public override bool CanExecute(object parameter)
        {
            if (parameter is GameViewModel gameViewModel)
            {
                //we make sure that the game view model is not null
                //so we don't subscribe the function to the property changed event
                //multiple times
                if (_isAssignedToGame == false)
                {
                    _isAssignedToGame = true;
                    gameViewModel.PropertyChanged += GameViewModel_PropertyChanged;
                }
            }
            return base.CanExecute(parameter);
        }

        //we check if the gameOver property changed to true and execute the main menu command if yes
        private void GameViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            GameViewModel gameViewModel = sender as GameViewModel;
            if (e.PropertyName == nameof(GameViewModel.IsGameOver) && gameViewModel.IsGameOver)
            {
                Execute(sender);
            }
        }
    }
}
