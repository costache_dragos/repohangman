﻿using Hangman.Models;
using Hangman.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class LoadGameCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        private LoadGameViewModel LoadGameViewModel { get; set; }
        
        public LoadGameCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }
        
        public override void Execute(object parameter)
        {
            if(parameter is LoadGameViewModel loadGameViewModel)
            {
                if (loadGameViewModel.SelectedSave == null)
                    return;
                
                _mainWindowViewModel.CurrentViewModel = new GameViewModel()
                {
                    CurrentUser = loadGameViewModel.CurrentUser,
                    Game = loadGameViewModel.SelectedSave.SavedGame
                };
                _mainWindowViewModel.WindowWidth = Constants.GameWindowSize.windowWidth;
                _mainWindowViewModel.WindowHeight = Constants.GameWindowSize.windowHeight;
            }
        }

        public override bool CanExecute(object parameter)
        {
            LoadGameViewModel childViewModel = parameter as LoadGameViewModel;
            
            //to avoid subscribing OnViewModelPropertyChanged every time that CanExecute is called
            //we asing the ProfileSelectorViewModel property the value of the child view model
            if (childViewModel != null && LoadGameViewModel == null)
            {
                childViewModel.PropertyChanged += OnViewModelPropertyChanged;
                LoadGameViewModel = childViewModel;
            }

            return childViewModel?.SelectedSave != null && base.CanExecute(parameter);
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LoadGameViewModel.SelectedSave))
                OnCanExecuteChanged();
        }
    }
}
