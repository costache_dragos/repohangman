﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class NavBackToGameFromSave : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        public NavBackToGameFromSave(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }

        public override void Execute(object parameter)
        {
            if (parameter is SaveGameViewModel saveGameViewModel)
            {
                _mainWindowViewModel.CurrentViewModel = new GameViewModel()
                {
                    CurrentUser = saveGameViewModel.CurrentUser,
                    Game = saveGameViewModel.GameToBeSaved
                };
            }
        }
    }
}
