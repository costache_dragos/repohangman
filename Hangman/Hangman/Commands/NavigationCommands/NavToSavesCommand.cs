﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class NavToSavesCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        public NavToSavesCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }
        
        public override void Execute(object parameter)
        {
            if(parameter is GameViewModel gameViewModel)
            {
                gameViewModel.Timer.Stop();
                _mainWindowViewModel.CurrentViewModel = new SaveGameViewModel()
                {
                    CurrentUser = gameViewModel.CurrentUser,
                    GameToBeSaved = gameViewModel.Game
                };
            }
        }
    }
}
