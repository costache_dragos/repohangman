﻿using Hangman.ViewModels;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class NewGameCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        public NewGameCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }

        public override void Execute(object parameter)
        {
            if (parameter is MainMenuViewModel mainMenuViewModel)
            {
                _mainWindowViewModel.CurrentViewModel = new GameCategorySelectorViewModel()
                {
                    CurrentUser = mainMenuViewModel.CurrentUser
                };
                _mainWindowViewModel.WindowWidth = Constants.GameWindowSize.windowWidth;
                _mainWindowViewModel.WindowHeight = Constants.GameWindowSize.windowHeight;
            }

        }
    }
}
