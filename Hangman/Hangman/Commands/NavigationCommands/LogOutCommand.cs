﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class LogOutCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        public LogOutCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }
        
        public override void Execute(object parameter)
        {
            _mainWindowViewModel.CurrentViewModel = new ProfileSelectorViewModel();
        }
    }
}
