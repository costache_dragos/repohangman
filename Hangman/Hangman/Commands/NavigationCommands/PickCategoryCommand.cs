﻿using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class PickCategoryCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        public PickCategoryCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }

        //the function gets a Game category selector viewModel
        //and uses the selected category to create a new game
        public override void Execute(object parameter)
        {
            if (parameter is GameCategorySelectorViewModel gameCategorySelectorViewModel)
            {
                //if no category is selected, do nothing
                if (gameCategorySelectorViewModel.SelectedCategory == null)
                    return;
                
                var category = gameCategorySelectorViewModel.SelectedCategory;
                _mainWindowViewModel.CurrentViewModel = new GameViewModel
                {
                    Game = new GameModel(category),
                    CurrentUser = gameCategorySelectorViewModel.CurrentUser
                };
            }
        }
    }
}
