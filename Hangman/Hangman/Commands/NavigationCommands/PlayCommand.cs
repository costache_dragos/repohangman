﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    //the command takes us from the profile selector to the main menu
    //it recieves a profileSelectorViewModel as a parameter
    //from which it gets the selected user profile 
    public class PlayCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainViewModel;
        private ProfileSelectorViewModel ProfileSelectorViewModel { get; set; }
        
        public PlayCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainViewModel = mainWindowViewModel;
        }

        public override void Execute(object parameter)
        {
            if (parameter is ProfileSelectorViewModel profileSelectorViewModel)
            {
                MainMenuViewModel mainMenuViewModel = new MainMenuViewModel()
                {
                    CurrentUser = profileSelectorViewModel.SelectedUser
                };
                _mainViewModel.CurrentViewModel = mainMenuViewModel;
            }
        }

        public override bool CanExecute(object parameter)
        {
            ProfileSelectorViewModel childViewModel = parameter as ProfileSelectorViewModel;

            //to avoid subscribing OnViewModelPropertyChanged every time that CanExecute is called
            //we asing the ProfileSelectorViewModel property the value of the child view model
            if (childViewModel != null && ProfileSelectorViewModel == null)
            {
                childViewModel.PropertyChanged += OnViewModelPropertyChanged;
                ProfileSelectorViewModel = childViewModel;
            }

            return childViewModel?.SelectedUser != null && base.CanExecute(parameter);
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ProfileSelectorViewModel.SelectedUser))
                OnCanExecuteChanged();
        }
    }
}
