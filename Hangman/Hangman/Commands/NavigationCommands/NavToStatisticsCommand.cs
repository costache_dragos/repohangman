﻿using Hangman.Models;
using Hangman.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class NavToStatisticsCommand : CommandBase
    {
        MainWindowViewModel _mainWindowViewModel;

        public NavToStatisticsCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }
        
        public override void Execute(object parameter)
        {
            if(parameter is MainMenuViewModel mainMenuViewModel)
            {
                _mainWindowViewModel.CurrentViewModel = new StatisticsViewModel()
                {
                    CurrentUser = mainMenuViewModel.CurrentUser
                };
            }
        }
    }
}
