﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class MainMenuLoadGameCommand : CommandBase
    {
        private readonly MainWindowViewModel _mainWindowViewModel;
        public MainMenuLoadGameCommand(MainWindowViewModel mainWindowViewModel)
        {
            _mainWindowViewModel = mainWindowViewModel;
        }
        public override void Execute(object parameter)
        {
            if(parameter is MainMenuViewModel mainMenuViewModel)
            {
                _mainWindowViewModel.CurrentViewModel = new LoadGameViewModel()
                {
                    CurrentUser = mainMenuViewModel.CurrentUser
                };
            }
        }
    }
}
