﻿using Hangman.Models;
using Hangman.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hangman.Commands
{
    public class CreateNewSaveCommand : CommandBase
    {
        private readonly NewSaveWindowViewModel _newSaveWindowViewModel;
        public CreateNewSaveCommand(NewSaveWindowViewModel newSaveWindowViewModel)
        {
            _newSaveWindowViewModel = newSaveWindowViewModel;
            _newSaveWindowViewModel.PropertyChanged += OnViewModelPropertyChanged;
        }

        public override void Execute(object parameter)
        {
            string saveDirPath = Utility.UsernameToUserSaveGamesDirPath
                (_newSaveWindowViewModel.SaveGameViewModel.CurrentUser.Username);
            if (string.IsNullOrEmpty(saveDirPath))
                return;

            //create the user directory(nothing happens if it already exists)
            System.IO.Directory.CreateDirectory(saveDirPath);
            
            //check if the name of the save file is valid
            if(!Utility.CheckIfSaveFileNameIsValid(_newSaveWindowViewModel.InputSaveName))
            {
                MessageBox.Show("Invalid save file name. Please try again.");
                return;
            }

            string saveFilePath = Path.Combine(saveDirPath, $"{_newSaveWindowViewModel.InputSaveName}.save");
            if (System.IO.File.Exists(saveFilePath))
            {
                MessageBox.Show("A save with that name already exists. Please choose a different name.", 
                    "Save already exists", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //create a json format using json.net
            string saveContent = JsonConvert.SerializeObject
                (_newSaveWindowViewModel.SaveGameViewModel.GameToBeSaved);
            
            //write the save file in the user directory
            System.IO.File.WriteAllText(saveFilePath, saveContent);

            //add a new save to the save list from the parent viewModel
            _newSaveWindowViewModel.SaveGameViewModel.SavedGames.Add(
                new SaveModel()
                {
                    Name = _newSaveWindowViewModel.InputSaveName,
                    SavedGame = _newSaveWindowViewModel.SaveGameViewModel.GameToBeSaved,
                    LastWriteTime = (new FileInfo(saveFilePath)).LastWriteTime.ToString()
                }
                );

            //close the window
            MessageBox.Show("Game saved successfully!", "Saved", MessageBoxButton.OK, MessageBoxImage.Information);
            _newSaveWindowViewModel.CloseWindow();
        }

        public override bool CanExecute(object parameter)
        {
            return !string.IsNullOrEmpty(_newSaveWindowViewModel.InputSaveName) && base.CanExecute(parameter);
        }

        private void OnViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(NewSaveWindowViewModel.InputSaveName))
                OnCanExecuteChanged();
        }
    }
}
