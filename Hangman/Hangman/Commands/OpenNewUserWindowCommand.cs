﻿using Hangman.ViewModels;
using Hangman.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Commands
{
    public class OpenNewUserWindowCommand : CommandBase
    {
        private readonly ProfileSelectorViewModel _viewModel;
        public OpenNewUserWindowCommand(ProfileSelectorViewModel profileSelectorViewModel)
        {
            _viewModel = profileSelectorViewModel;
        }

        public override void Execute(object parameter)
        {
            NewUserWindowView newUserView = new NewUserWindowView(_viewModel);
            newUserView.ShowDialog();
        }
    }
}
